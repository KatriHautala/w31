import React from "react";
import "./Tori.css";

const Tori = () => {
  return (
    <div className="tori">
        <input type="text" value="Hakusana ja/tai postinumero" class="inputbox"/>
        <select name="what" class="option1">
          <option id="1" >Kaikki osastot</option>
        </select>
        <select name="where" class="option2">
          <option id="1" >Koko Suomi</option>
        </select>
        <br/>
        <br/>
        <label class="form-control">
          <input type="checkbox" name="checkbox"/>
          Myydään
        </label>
        <label class="form-control">
          <input type="checkbox" name="checkbox"/>
          Ostetaan
        </label>
        <label class="form-control">
          <input type="checkbox" name="checkbox"/>
          Vuokrataan
        </label>
        <label class="form-control">
          <input type="checkbox" name="checkbox"/>
          Halutaan vuokrata
        </label>
        <label class="form-control">
          <input type="checkbox" name="checkbox"/>
          Annetaan
        </label>
        <label class="save">Tallenna haku</label>
        <button className="button">Hae</button>
    </div>
  );
};

export default Tori;
